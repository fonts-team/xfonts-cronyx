#!/bin/sh

# Prepare a distribution registered with iso8859-5. # rawin-r.

# Directory to prepare files in.

. version.sh

targetdir="./xcyr-$version-cp1251"

# Prepare that directory.

./mktargetdir.sh "$targetdir"

# Transform all fonts.

echo Recoding fonts:
for font in misc/*.bdf 100dpi/*.bdf 75dpi/*.bdf
do
	name=`expr $font : 'misc/koi\(.*bdf\)'`
    perl bdfrecode.pl -table=xcyr2cp1251.txt -registry=RAWIN-R < "$font" > "$targetdir"/$font
	[ x"$name" != "x" ] && mv "$targetdir/$font" "$targetdir/misc/win$name"
    echo "$font"
done

# Transform "fonts.alias" in the "misc" directory
perl -e 'while(<>){s/koi8-c/RAWIN-R/g;s/^koi/win/g;print if (/^win/);}' < misc/fonts.alias > "$targetdir"/misc/fonts.alias

# Transform the special font "koinil2.bdf"

perl bdfrecode.pl -table=full.table -registry=RAWIN-R < misc/koinil2.bdf > $targetdir/misc/winnil2.bdf

# Transform "Makefile" in the "misc" directory

perl -e 'while(<>){s/koi/win/g;print;}' < misc/Makefile > "$targetdir"/misc/Makefile

# Transform "install.sh" to install into "cyrillic-cp1251" by default

for a in version.sh DEBIAN/*
do
	cat "$a" | sed -e 's/xcyr/xcyr-cp1251/g;s/koi8c/cp1251/g' > "$targetdir"/"$a"
done

cat DEBIAN/control | sed -e 's/cyrillic-koi8c/cyrillic-cp1251/;s/old Russian/Cyrillic/;s/a special KOI8-C/the CP1251/;s/\(Description: .*\) KOI8-C encoding/\1 CP-1251 encoding/;s/xfntcyr/xnftcyr1251/;' > "$targetdir"/DEBIAN/control


echo Done.
