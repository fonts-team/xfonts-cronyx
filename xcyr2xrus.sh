#!/bin/sh

# Prepare a stripped-down xrus distribution.

. version.sh

# Directory to prepare xrus files in.
targetdir="./xrus-$version"

# Prepare that directory.

./mktargetdir.sh "$targetdir"

cp * "$targetdir"

rm "$targetdir"/*.sh "$targetdir"/xcyr*

# Transform all fonts.

echo Recoding fonts:
for font in misc/*.bdf
do
    perl bdfrecode.pl -table=xcyr2xrus.txt -registry=KOI8-R < $font > $targetdir/$font
    echo "$font"
done
for font in 100dpi/*.bdf 75dpi/*.bdf
do
    perl bdfrecode.pl -table=xcyr2xrus.txt -registry=KOI8-R < $font > $targetdir/$font
    echo "$font"
done

# Transform "fonts.alias" in the "misc" directory
perl -e 'while(<>){s/koi8-c/koi8-r/g;print;}' < misc/fonts.alias > "$targetdir"/misc/fonts.alias

# Transform the special font "koinil2.bdf"

perl bdfrecode.pl -table=full.table -registry=KOI8-R < misc/koinil2.bdf > $targetdir/misc/koinil2.bdf

echo Done.
