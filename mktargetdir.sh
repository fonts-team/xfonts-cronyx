#!/bin/sh

# Make target directory

rm -rf "$1"

mkdir -p "$1"

# Copy stuff there.

cp -r 75dpi 100dpi misc DEBIAN Changelog* *README* *.pl makedeb.sh version.sh install.sh "$1"
 
