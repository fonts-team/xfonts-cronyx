#!/bin/sh

# Prepare a distribution registered with iso8859-5.

. version.sh

# Directory to prepare files in.
targetdir="./xcyr-$version-iso"

# Prepare that directory.

./mktargetdir.sh "$targetdir"

# Transform all fonts.

echo Recoding fonts:
for font in misc/*.bdf 100dpi/*.bdf 75dpi/*.bdf
do
	name=`expr $font : 'misc/koi\(.*bdf\)'`
    perl bdfrecode.pl -table=xcyr2iso.txt -registry=ISO8859-5 < "$font" > "$targetdir"/$font
	[ x"$name" != "x" ] && mv "$targetdir/$font" "$targetdir/misc/iso$name"
    echo "$font"
done

# Transform "fonts.alias" in the "misc" directory
perl -e 'while(<>){s/koi8-c/iso8859-5/g;s/^koi/iso/g;print if (/^iso/);}' < misc/fonts.alias > "$targetdir"/misc/fonts.alias

# Transform the special font "koinil2.bdf"

perl bdfrecode.pl -table=full.table -registry=ISO8859-5 < misc/koinil2.bdf > $targetdir/misc/isonil2.bdf

# Transform "Makefile" in the "misc" directory

perl -e 'while(<>){s/koi/iso/g;print;}' < misc/Makefile > "$targetdir"/misc/Makefile

# Transform "install.sh" to install into "cyrillic-iso" by default

perl -e 'while(<>){s/(targetdir=.*cyrillic)/\1-iso/;print;}' < install.sh > "$targetdir"/install.sh

echo Done.




