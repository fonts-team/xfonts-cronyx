define(`what_it_supports_macro',`ifelse(`$1',`', `and support all current slavic Cyrillic letters as well as some Russian prerevolutionary letters.',`$1',`-koi8r',`and are suitable for Russian.',`$1',`-koi8u',`and are suitable for Ukrainian.',`$1',`-isocyr',`and support the Belarusian, Bulgarian, Macedonian, Russian and Serbian scripts.', `and support all current slavic Cyrillic scripts.')')dnl
Source: xfonts-cronyx
Maintainer: Anton Zinoviev <zinoviev@debian.org>
Section: x11
Priority: optional
Standards-Version: 3.5.2.0
Build-Depends: debhelper (>=3), trscripts (>= 1.10), xutils
define(`cronyx100dpi', `
Package: xfonts-cronyx$1-100dpi
Architecture: all
Description: 100 dpi $2 Cyrillic fonts for X (Cronyx collection)
 This package provides a set of bitmapped Cyrillic fonts at 100 dots
 per inch.  These are $3 fonts
 what_it_supports_macro($1)
 In most cases it is desirable to have the X font server (xfs) and/or
 an X server installed to make the fonts available to X clients.
 .
 This package and xfonts-cronyx$1-75dpi provide the same set of
 fonts, rendered at different resolutions; only one or the other is
 necessary, but both may be installed.  xfonts-cronyx$1-100dpi may be
 more suitable for large monitors and/or large screen resolutions
 (over 1024x768).
 .
 The upstream source of this package is the package `Xcyr' of Serge
 Winitzki.  `Xcyr' is a descendant of the package `Xrus-2.3' of Andrey
 Chernov.
Section: x11
Priority: optional
Replaces: xfonts-cronyx-$4
Depends: xutils (>= 4.0.2-11)
Suggests: xfs | xserver
Conflicts: xfonts-cronyx-$4')dnl
define(`cronyx75dpi', `
Package: xfonts-cronyx$1-75dpi
Architecture: all
Description: 75 dpi $2 Cyrillic fonts for X (Cronyx collection)
 This package provides a set of bitmapped Cyrillic fonts at 75 dots
 per inch.  These are $3 fonts
 what_it_supports_macro($1)
 In most cases it is desirable to have the X font server (xfs) and/or
 an X server installed to make the fonts available to X clients.
 .
 This package and xfonts-cronyx$1-100dpi provide the same set of
 fonts, rendered at different resolutions; only one or the other is
 necessary, but both may be installed.  xfonts-cronyx$1-75dpi may be
 more suitable for small monitors and/or small screen resolutions
 (under 1024x768).
 .
 The upstream source of this package is the package `Xcyr' of Serge
 Winitzki.  `Xcyr' is a descendant of the package `Xrus-2.3' of Andrey
 Chernov.
Section: x11
Priority: optional
Replaces: xfonts-cronyx-$4
Depends: xutils (>= 4.0.2-11)
Suggests: xfs | xserver
Conflicts: xfonts-cronyx-$4')dnl
define(`cronyxmisc', `
Package: xfonts-cronyx$1-misc
Architecture: all
Description: Character-cell $2 Cyrillic fonts for X (Cronyx collection)
 This package provides a standard set of character-cell low-resolution
 Cyrillic fonts.  These are $3 fonts
 what_it_supports_macro($1)
 In most cases it is desirable to have the X font server (xfs) and/or
 an X server installed to make the fonts available to X clients.
 .
 The upstream source of this package is the package `Xcyr' of Serge
 Winitzki.  `Xcyr' is a descendant of the package `Xrus-2.3' of Andrey
 Chernov.
Section: x11
Priority: optional
Replaces: xfonts-cronyx-$4
Depends: xutils (>= 4.0.2-11)
Suggests: xfs | xserver
Conflicts: xfonts-cronyx-$4')dnl
cronyx100dpi(`', `Unicode', `Unicode (ISO10646-1)', `nop')
cronyx100dpi(`-cp1251', `CP1251 encoded', `CP1251 (microsoft-cp1251)', `cp1251')
cronyx100dpi(`-isocyr', `ISO 8859-5 encoded', `ISO 8859-5', `iso')
cronyx100dpi(`-koi8r', `KOI8-R encoded', `KOI8-R', `koi8r')
cronyx100dpi(`-koi8u', `KOI8-U encoded', `KOI8-U', `koi8u')
cronyx75dpi(`', `Unicode', `Unicode (ISO10646-1)', `nop')
cronyx75dpi(`-cp1251', `CP1251 encoded', `CP1251 (microsoft-cp1251)', `cp1251')
cronyx75dpi(`-isocyr', `ISO 8859-5 encoded', `ISO 8859-5', `iso')
cronyx75dpi(`-koi8r', `KOI8-R encoded', `KOI8-R', `koi8r')
cronyx75dpi(`-koi8u', `KOI8-U encoded', `KOI8-U', `koi8u')
cronyxmisc(`', `Unicode', `Unicode (ISO10646-1)', `nop')
cronyxmisc(`-cp1251', `CP1251 encoded', `CP1251 (microsoft-cp1251)', `cp1251')
cronyxmisc(`-isocyr', `ISO-8859-5 encoded', `ISO-8859-5', `iso')
cronyxmisc(`-koi8r', `KOI8-R encoded', `KOI8-R', `koi8r')
cronyxmisc(`-koi8u', `KOI8-U encoded', `KOI8-U', `koi8u')
