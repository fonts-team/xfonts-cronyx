xfonts-cronyx (2.3.8-8) unstable; urgency=medium

  * Use debhelper compat level 9.  Thanks to Lucas Nussbaum,
    closes: #830390.

 -- Anton Zinoviev <zinoviev@debian.org>  Sun, 31 Jul 2016 21:02:44 +0300

xfonts-cronyx (2.3.8-7) unstable; urgency=medium

  * Make the build reproducible.  Thanks to Chris Lamb, closes: #778230.

 -- Anton Zinoviev <zinoviev@debian.org>  Sun, 31 Jul 2016 18:30:23 +0300

xfonts-cronyx (2.3.8-6) unstable; urgency=low

  * Depend on xfonts-utils instead of xutils.  Thanks to Guido Guenther,
    closes: #363268.
  * Remove the hack to work around #362820.  Build-depend on debhelper (>=
    5.0.32)

 -- Anton Zinoviev <zinoviev@debian.org>  Wed, 19 Apr 2006 11:45:00 +0300

xfonts-cronyx (2.3.8-5) unstable; urgency=low

  * Updated for the new modular X.  Closes: #362357, #362358, #362359, #362361, #362362, #362364, #362365, #362366, #362368, #362369, #362370, #362372, #362373, #362374, #362375
  * Updated standards version (3.6.6.2).
  
 -- Anton Zinoviev <zinoviev@debian.org>  Sat, 15 Apr 2006 23:15:09 +0300

xfonts-cronyx (2.3.8-4) unstable; urgency=low

  * Reencoded using trbdf version 1.11.  The pseudographic symbols are
    approximated by termsymbols with codes <=0x1f when appropriate.
    Thanks to Victor B. Wagner.
  * Updated `Standards-Version': 3.5.10.
  * The files in /etc/X11/fonts are not executables any more. ;-)
	
 -- Anton Zinoviev <zinoviev@debian.org>  Sat,  7 Jun 2003 22:40:36 +0200

xfonts-cronyx (2.3.8-3) unstable; urgency=low

  * The upstream sent me new version of koi7x14.bdf and crox1c.bdf.  Thus
    you can consider this Debian release as a new upstream version.

 -- Anton Zinoviev <zinoviev@debian.org>  Wed, 15 Apr 2003 23:12:39 +0300

xfonts-cronyx (2.3.8-2) unstable; urgency=low

  * The new version of trbdf always sets DEFAULT_CHAR 32.  In the
    original `misc' fonts there was DEFAULT_CHAR 0 which doesn't exists
    (Thanks to Alexandra N. Kossovsky, closes: #170413).
  * Used -cronyx- foundry instead of -misc- in fonts.alias (thanks to
    Alexander Kotelnikov for pointing me the bug).
  * $(call foreachenc,...) and $(call foreachcat,...) eliminated from
    debian/rules.  This is workaround #175714.  Thanks to Daniel Schepler.
  * Used unicode_small in trbdf to make the unicode fonts use smaller
    memory space.
  * Used `Build-Depends' instead of `Build-Depends-Indep'.
	
 -- Anton Zinoviev <zinoviev@debian.org>  Thu, 14 Jan 2003 11:10:54 +0200

xfonts-cronyx (2.3.8-1) unstable; urgency=low

  * New upstream version.

 -- Anton Zinoviev <zinoviev@debian.org>  Wed,  7 Aug 2002 11:52:57 +0300

xfonts-cronyx (2.3.7-9) unstable; urgency=low

  * Build-Depends-Indep: trscrips (>= 1.5).  This makes pseudographic
    characters available in all fixed-width fonts.
  * Added option -m to install in rules file.

 -- Anton Zinoviev <zinoviev@debian.org>  Mon, 28 Jan 2002 23:00:13 +0200

xfonts-cronyx (2.3.7-8) unstable; urgency=low

  * New binary package xfonts-cronyx with fonts for Unicode.

 -- Anton Zinoviev <zinoviev@debian.org>  Sun, 17 Jun 2001 14:52:08 +0300

xfonts-cronyx (2.3.7-7) unstable; urgency=low

  * Double exclamation signs in *.alias file.

 -- Anton Zinoviev <zinoviev@debian.org>  Tue, 17 Apr 2001 13:37:24 +0300

xfonts-cronyx (2.3.7-6) unstable; urgency=low

  * A complete remake and Debconf in use.
  * The packages xfonts-cronyx-${ENC} are splitted in three packages:
    xfonts-cronyx-${ENC}-misc, xfonts-cronyx-${ENC}-100dpi and
    xfonts-cronyx-${ENC}-75dpi.  This is a requirement of the latest
    Debian policy.
  * Standards-Version: 3.5.2.0
  * Few fonts again changed their foundry field.

 -- Anton Zinoviev <zinoviev@debian.org>  Thu, 29 Mar 2001 19:00:31 +0200

xfonts-cronyx (2.3.7-5) unstable; urgency=medium

  * The foundry field `misc' in all fonts is replaced with `cronyx' --
    this is needed to avoid collision with fonts from `xfonts-base'
    package.  Sorry.
  * The font `-Misc-Fixed-Medium-R-Normal--20-200-75-75-C-100-KOI8-C' is
    renamed: `-Misc-Fixed-Medium-R-Normal--19-190-75-75-C-100-KOI8-C'.
  * The packages install directories in /etc/X11/fonts/ even in case of
    missing fonts.alias.
  * Fixed a GRAVE bug in `postrm' -- the previous version was
    uninstallable.

 -- Anton Zinoviev <zinoviev@debian.org>  Sat, 30 Dec 2000 21:11:03 +0200

xfonts-cronyx (2.3.7-4) unstable; urgency=low

  * The fonts for KOI8-R was installed in the directories
    koi8-r.{misc,100dpi,75dpi}, not in KOI8-R.{...}.  This was a bug in
    the changelog file.
  * The file README.Debian is removed from the source package.

 -- Anton Zinoviev <zinoviev@debian.org>  Sun, 19 Nov 2000 16:00:31 +0200

xfonts-cronyx (2.3.7-3) unstable; urgency=low

  * The fonts for KOI8-R are installed in their own directories
    KOI8-R.misc, KOI8-R.75dpi and KOI8-R.100dpi.

 -- Anton Zinoviev <zinoviev@debian.org>  Thu, 16 Nov 2000 12:16:41 +0200

xfonts-cronyx (2.3.7-2) unstable; urgency=low

  * The fonts are installed in the standard directories
    /usr/X11R6/lib/X11/fonts/{misc,75dpi,100dpi}.
  * New fonts.alias is used.  (There was colision in the names with other
    fonts.aliases.
  * No package with fonts for ECMA-Cyrillic.
  
 -- Anton Zinoviev <anton@lml.bas.bg>  Tue,  3 Oct 2000 16:12:04 +0300

xfonts-cronyx (2.3.7-1) unstable; urgency=low

  * New upstream version: used the package xcyr-2.3.7 of Serge Winitzki
    instead of xrus-2.3.1.  The encoding of the fonts in xcyr is KOI8-C.
    They have more Cyrillic letters than fonts of xrus.
  * Used new version of trbdf that supports KOI8-C.
  * New package with fonts for ECMA-Cyrillic.

 -- Anton Zinoviev <anton@lml.bas.bg>  Sat, 10 Jun 2000 11:48:58 +0300

xfonts-cronyx (2.3.1-2) unstable; urgency=low

  * New version of trbdf is used.  This allowed generating of KOI8-U
    fonts.
  * The documentation is moved to /usr/share/doc
  * trbdf is used also for KOI8-R fonts.

 -- Anton Zinoviev <anton@lml.bas.bg>  Sun,  4 Jun 2000 16:23:50 +0300

xfonts-cronyx (2.3.1-1) unstable; urgency=low

  * New upstream version.

 -- Anton Zinoviev <anton@lml.bas.bg>  Wed,  3 May 2000 20:43:46 +0300

xfonts-cronyx (2.2.0-1) unstable; urgency=low

  * Initial release.
  * Fonts converted to various encodings by `trbdf' script.

 -- Anton Zinoviev <anton@lml.bas.bg>  Fri,  3 Mar 2000 13:09:01 +0200
