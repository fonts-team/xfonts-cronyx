#!/usr/bin/perl
# Turn any BDF font with a known encoding into one with ISO10646-1
# encoding.
# Markus Kuhn <mkuhn@acm.org> -- 1999-07-14

# Changes by Serge Winitzki

if ($#ARGV < 0) {

	print <<"End";
Usage: any2ucs <source-name> [mapping-file]

where

   <source-name>        is the name of the input BDF file
   <mapping-file>       is the name of the encoding file

The output file name will have "-ucs" inserted before ".bdf". If the
encoding file is not given, the encoding table corresponding to the
font encoding must be present in current directory e.g. "KOI8-C" file.
End

	exit;
}

# Open input and output file
$fin = $ARGV[0];
$fout = $fin;
$fout =~ s/.bdf/-ucs.bdf/;
if ($fout eq $fin) {
    $fout .= "-ucs";
}
open(FIN,  "<$fin")  || die ("Can't read from '$fin'.\n");

# Read and modify header
$header = "";
$charset_registry = "";
$charset_encoding = "";
while (<FIN>) {
    last if /^CHARS\s+/;
    if (/^CHARSET_REGISTRY\s+"(.*)"\s*$/) {
	$charset_registry = $1;
	$header .= "CHARSET_REGISTRY \"ISO10646\"\n";
    } elsif (/^CHARSET_ENCODING\s+"(.*)"\s*$/) {
	$charset_encoding = $1;
	$header .= "CHARSET_ENCODING \"1\"\n";
    } elsif (/^(FONT\s+.*)-[^-]*-[^-]*$/) {
	$header .= "$1-ISO10646-1\n";
    } else {
	if (/SPACING\s+\"?([cpmCPM])\"?\s*$/) {
	    $spacing = lc($1);
	}
	$header .= $_;
    }
}

# Read mapping table
$fnmap="";
$mapdir=".";
if ($ARGV[1] ne "") {
	$fnmap=$ARGV[1];
} else {
    $fnmap = "$mapdir/$charset_registry-$charset_encoding";
}
if ("$charset_registry-$charset_encoding" eq "JISX0208.1983-0") {
    $fnmap = "$mapdir/JIS0208.TXT" unless ($fnmap ne "");
    open(FMAP,  "<$fnmap")  || die ("Can't read from '$fnmap'.\n");
    while (<FMAP>) {
	next if /^\s*\#/;
	if (/^0x([0-9A-F]{4})\s+0x([0-9A-F]{4})\s+0x([0-9A-F]{4})\s+#/) {
	    $ucs{hex($2)} = hex($3);
	} else {
	    die("Mapping file syntax error:\n$_");
	}
    }
    close FMAP;
} else {
    open(FMAP,  "<$fnmap")  || die ("Can't read from '$fnmap'.\n");
    while (<FMAP>) {
	next if /^\s*\#/;
	tr/a-z/A-Z/;           # make input uppercase
	if (/^0X([0-9A-F]{2})\s+0X([0-9A-F]{4})\s+#/) {
	    $ucs{hex($1)} = hex($2);
	} else {
	    die("Mapping file syntax error:\n$_");
	}
    }
    close FMAP;
}

# Read characters
while (<FIN>) {
    if (/^STARTCHAR/) {
	if (/^STARTCHAR\s+([0-9A-Za-z._'-]+)$/) {
	    $name = $1;
	} else {
	    die("STARTCHAR syntax error:\n$_");
	}
    } elsif (/^ENCODING\s+(-?\d+)/) {
	$code = $1;
	if ($ucs{$code}) {
	    $char{$code} = sprintf("STARTCHAR uni%04X\n", $ucs{$code});
	    $char{$code} .= "ENCODING $ucs{$code}\n";
	} else {
	    $char{$code} = "STARTCHAR $name\n";
	    $char{$code} .= "ENCODING -1\n";
	    print STDERR  "Warning: Unknown character " .
		"'$name' with code $code in '$fin'.\n";
	}
    } elsif (/^ENDFONT\s+/) {
	$code = -1;
    } else {
	$char{$code} .= $_;
	if (/^ENDCHAR$/) {
	    $code = -1;
	}
    }
}

close FIN;

# Write output
open(FOUT, ">$fout") || die ("Can't write to '$fout'.\n");
print FOUT $header;
@chars = sort(keys(%char));
print FOUT "CHARS $#chars\n";

# Write characters
for $ucs (@chars) {
    print FOUT $char{$ucs};
}
print FOUT "ENDFONT\n",
