#!/bin/sh

# Compile and install all fonts from xrus-2.3 distribution.

#### Customize here

. version.sh

# 0. Font directories in this release.

fontdirs="75dpi 100dpi misc"
cronyxfontdirs="75dpi 100dpi"	# These should be made bold/oblique

# 1. How to compile the fonts. The usual command is "bdftopcf".

bdftopcf="bdftopcf"
termfontsflag="-t"	# Need "bdftopcf -t" for fixed fonts

# 2. How to compress the fonts.
# Most Linux X servers accept .pcf.gz fonts, but some require .pcf.Z.

compress="gzip -9 -f"	# Default for Linux. Create .pcf.gz fonts.

# compress="compress -f"	# If your system requires .pcf.Z fonts.

# 3. Where to install the fonts. Maybe /etc/X11/fonts/cyrillic or /usr/X11R6/lib/X11R6/fonts/cyrillic

targetdir="/usr/X11/lib/X11/fonts/$packagedir"

# 4. Temporary location to create installable fonts files, so that we can
# still compile the fonts even if we are not permitted to write to targetdir.

temptargetdir="./$packagedir"

# Subdirectory for BDF source files to be created.

source="source"

#### Nothing to customize below.

# 0. Prepare working directories.

origdir="`pwd`"


# 1. Generate bold, italic and bold-italic font sources and put all sources in the source/ directory.

[ x"$1" != xgenerated ] && { # For debugging purposes: `install.sh generated` will skip this step

# Clean up any previously generated fonts
rm -rf $temptargetdir
mkdir -p $temptargetdir
cd $temptargetdir
mkdir -p $fontdirs $source
cd $source
mkdir -p $fontdirs
cd $origdir

echo "Generating bold/oblique versions:"

for basedir in $cronyxfontdirs
do
  for fontfile in $basedir/*.bdf
  do
    fontname=`expr $fontfile : '.*/\(.*\).bdf$'`	# e.g. "crox1t"
    echo "$basedir/$fontname -> ${fontname}{o,b,bo}"
    cp $fontfile $temptargetdir/$source/$fontfile
	cd $temptargetdir/$source
    perl $origdir/bdfslant.pl -unjag > $basedir/${fontname}o.bdf < $fontfile
    perl $origdir/bdfbold.pl > $basedir/${fontname}b.bdf < $fontfile
    perl $origdir/bdfslant.pl -unjag > $basedir/${fontname}bo.bdf < $basedir/${fontname}b.bdf
    cd $origdir
  done
done

cp misc/*.bdf $temptargetdir/$source/misc

}

# 2. Compile and compress all resulting fonts.

echo "Compiling fonts..."

for basedir in $fontdirs
do
  #Determine whether we need the extra flag
  if [ "$basedir" = "misc" ]; then
    compilecommand="$bdftopcf $termfontflag"
  else
    compilecommand="$bdftopcf"   	
  fi
  for fontfile in $temptargetdir/$source/$basedir/*.bdf
  do
    fontname=`expr $fontfile : '.*/\(.*\).bdf$'`	# e.g. "crox1t"
    $compilecommand < $fontfile > $temptargetdir/$basedir/$fontname.pcf
    $compress $temptargetdir/$basedir/$fontname.pcf
  done
done

# 3. Copy any fonts.alias files.

for basedir in $fontdirs
do
    cp $basedir/fonts.* $temptargetdir/$basedir
done

# 4. Make font directories.

echo "Making font directories..."

cd $temptargetdir
for basedir in $fontdirs
do
  cd $basedir
  mkfontdir
  cd ..
done

cd $origdir

echo "Fonts are ready in $temptargetdir/"

# Make tar.gz of compiled fonts

mv $temptargetdir/$source .

tar cf - $temptargetdir/ | gzip > $packagename-$version.tar.gz

mv $source $temptargetdir

# 5. Install fonts into target directory.

echo "Attempting to install in $targetdir"
for basedir in $fontdirs
do
    cp -r $temptargetdir/$basedir $targetdir
done

echo "Installation completed."

