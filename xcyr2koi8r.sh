#!/bin/sh

# Prepare a distribution registered with koi8-r.

. version.sh

# Directory to prepare xrus files in.
targetdir="./xcyr-$version-koi8r"

# Prepare that directory.

./mktargetdir.sh "$targetdir"

# Transform all fonts.

echo Recoding fonts:
for font in misc/*.bdf 100dpi/*.bdf 75dpi/*.bdf
do
    perl bdfrecode.pl -table=full.table -registry=KOI8-R < $font > $targetdir/$font
    echo "$font"
done

# Transform "fonts.alias" in the "misc" directory
perl -e 'while(<>){s/koi8-c/koi8-r/g;print;}' < misc/fonts.alias > "$targetdir"/misc/fonts.alias

# Transform the special font "koinil2.bdf"

#perl bdfrecode.pl -table=full.table -registry=KOI8-R < misc/koinil2.bdf > $targetdir/misc/koinil2.bdf

echo Done.




