#!/usr/bin/perl

# Compare bounding boxes for some characters in a font
# Usage: e.g. to compare characters 101, 163 and 197:
#	bdfcmpc.pl 101 163 197 < font.bdf

undef $/;

$_ = <STDIN>;	# Read font

$ok = 1;
for ($i=0; $i<=$#ARGV; ++$i) { 
	$charcode[$i] = $ARGV[$i];
	if (/\nENCODING\s*$charcode[$i]\nSWIDTH.*\nDWIDTH\s(\d*)\s.*\nBBX\s(\d*)\s(\d*)\s([-0-9]*)\s([-0-9]*)\n/) {
		($d[$i], $w[$i], $h[$i], $bbx[$i], $bby[$i]) = ($1, $2, $3, $4, $5);
		if ($i > 0 and ($d[$i-1] != $1 or $w[$i-1] != $2 or $bbx[$i-1] != $4)) {
			$c=pack("C", $charcode[$i]);
			print "char $charcode[$i] ($c): $1 $2 $4. ";
		}
	} else {
		print "No char $charcode[$i]. ";
		$ok = 0;
	}
}
